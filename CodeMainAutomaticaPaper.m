

%%%%%%%%%%%%%%%  Main function %%%%%%%%%%%%
% In order to run the code, just type CodeMainAutomaticaPaper in the 
% command prompt
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function CodeMainAutomaticaPaper


%%%%%%%%%%%%%%%%%
%INPUT PARAMETERS
%%%%%%%%%%%%%%%%%
Tsim = 5;   %Simulation time
Nu=35;      %Size of the input
n =34;      %Initial state lambda*sin(n*x*pi)
c = 1/30; 
lambda = 10;
uxt = @(x,t) exp(-n^2*pi^2*c^2*t)*sin(n*pi*x); %exact solution from the initial state for u=0
ux = @(x) uxt(x,0);   %initial state V_{opt}(0)

dim = 40;             %dimension of the projection

a0 = lambda*projection(dim,ux);  % initial state hatM_N^{-1}P_N V_{opt}(0)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Constructing the DAE-LTI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------------------------------------------
% Step 1:                                    Computing AN,LambdaN, hatM_N
%------------------------------------------------------------------------------------------------------------
M = mass_matrix_corrected(dim);          % M_N
S = stiffness_matrix_corrected(dim,c^2); %Lambda_N A_N  
scaling_Matrix = diag((2*[1:dim]'+1)/2); % Lambda_N
Mnoscaling = mass_matrix_corrected_no_conditioning(dim);  %hatM_N
AN = scaling_Matrix\S; 

save('MN_matrix','M');
matrix2latex('MN_matrix.tex',M);
save('AN_matrix', 'AN');
matrix2latex('AN_matrix.tex',AN);
save('LambdaN_matrix', 'scaling_Matrix');
matrix2latex('LambdaN_matrix.tex', scaling_Matrix);
save('hatMN_matrix', 'Mnoscaling');
matrix2latex('hatMN_matrix.tex',Mnoscaling);

%-----------------------------------------------------------------------------------------------------------
% Step 2:                                Computing A,E
%------------------------------------------------------------------------------------------------------
A=[S, scaling_Matrix];    
E=[M, zeros(dim,dim)]; 
save('A_matrix','A');
matrix2latex('A_matrix.tex',A);
save('E_matrix','E');
matrix2latex('E_matrix.tex',E);

%----------------------------------------------------------------------------------------------------------
% Step 3:                                    Computing B
%----------------------------------------------------------------------------------------------------------
B1 = zeros(dim,Nu);


for k=1:Nu
    for i = 1:dim
        B1(i,k)=inner_product(i,@(x) sin(k*x*pi),dim);    
    end    
end    

B=scaling_Matrix*B1;


save('B_matrix','B');
matrix2latex('B_matrix.tex', B, 'format', '%f');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                          Solving Problem 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------------
% Step 1:                 Constructing Problem 2
%--------------------------------------------------------------------------------------------------------
Q = blkdiag(Mnoscaling, 0.01*eye(dim));  %Matrices Q,R of Problem 2
R = eye(Nu);

save('Q_matrix_of_DAE_LTI_LQ','Q');
save('R_matrix_of_DAE_LTI_R','R');
matrix2latex('Q_matrix_of_DAE_LTI_LQ.tex',Q);
matrix2latex('R_matrix_of_DAE_LTI_R.tex',R);


%---------------------------------------------------------------------------------------------------------
% Step 2:     Computing associated ODE-LTI
%------------------------------------------------------------------------------------------------------------
[ss2lin, LMAP] =  DAE2Lin(E,A,B);               %Computing associated ODE-LTI

Al=ss2lin.a;
Bl=ss2lin.b;
Cl=ss2lin.c;
Dl=ss2lin.d;
save('AssociatedSystem_A_l', 'Al');
save('AssociatedSystem_B_l', 'Bl');
save('AssociatedSystem_C_l', 'Cl');
save('AssociatedSystem_D_l', 'Dl');
save('AssociatedSystemStateMap_M', 'LMAP');

matrix2latex('AssociatedSystem_A_l.tex', Al, 'format', '%f');
matrix2latex('AssociatedSystem_B_l.tex', Bl, 'format', '%f');
matrix2latex('AssociatedSystem_C_l.tex', Cl, 'format', '%f');
matrix2latex('AssociatedSystem_D_l.tex', Dl,  'format', '%f');
matrix2latex('AssociatedSystemStateMap_M.tex', LMAP, 'format', '%f');

%---------------------------------------------------------------------------------------
% Step 3:      Computing stabilizable associated ODE-LTI and 
%              solving algebraic Riccati equation
%---------------------------------------------------------------------------------------
[Kl,PS,sys_stab,SLMAP] = DAEOptContrInf(E,A,B,Q,R);  %Solving Problem 2

Ag=sys_stab.a;
Bg=sys_stab.b;
Cg=sys_stab.c;
Dg=sys_stab.d;
save('AssociatedStabilizableSystem_A_l', 'Ag');
save('AssociatedStabilizableSystem_B_l', 'Bg');
save('AssociatedStabilizableSystem_C_l', 'Cg');
save('AssociatedStabilizableSystem_D_l', 'Dg');
save('AssociatedStabilizableSystemStateMap_M', 'SLMAP');

save('MatrixPofAlegbraicRiccati','PS');
save('MatrixFeedbackKofAlgebraicRiccati','Kl');

matrix2latex('AssociatedStabilizableSystem_A_l.tex', Ag, 'format', '%f');
matrix2latex('AssociatedStabilizableSystem_B_l.tex', Bg, 'format', '%f');
matrix2latex('AssociatedStabilizableSystem_C_l.tex', Cg, 'format', '%f');
matrix2latex('AssociatedStabilizableSystem_D_l.tex', Dg, 'format', '%f' );
matrix2latex('AssociatedStabilizableSystemStateMap_M.tex', SLMAP, 'format', '%f');
matrix2latex('MatrixPofAlegbraicRiccati.tex',PS, 'format', '%f');
matrix2latex('MatrixFeedbackKofAlgebraicRiccati.tex',Kl, 'format', '%f');

%--------------------------------------------------------------------------------------
% Step 4:  Computing matrices K_1 and K_f
%------------------------------------------------------------------------------------
Ks=(sys_stab.C-sys_stab.D*Kl)*SLMAP;                  %Matrix K_1

save('MatrixK1','Ks');
matrix2latex('MatrixK1.tex',Ks,'format', '%f');

K=Ks(2*dim+1:end,:);

save('MatrixKf','K');
matrix2latex('MatrixKf.tex',K, 'format', '%f');

%-------------------------------------------------------------------------------------
% Step 5: Computing J(x_*,u_*) for the initial state Ex0=M_N hatMN^{-1} P_N V
%--------------------------------------------------------------------------------------

x0 = [a0; S*a0-(c^2*pi^2*n^2 * a0)];              %Initial state of the DAE-LTI

cost_DAE = (SLMAP*E*x0)'*PS*(SLMAP*E*x0);         %Optimal cost J_{infty}(x_*,u_*)

Q0=zeros(dim,dim);                                %Q0 zero


%--------------------------------------------------------------------------------------
% Step 6: Computing x_*,u_*
%--------------------------------------------------------------------------------------

% Compute x_*=xstates, u_*=ustates, J_T(x_*,u_*) 
[time,xstates,uvalues,cost] = SimulateClLoopDAEInfOpt(Kl,sys_stab,SLMAP, A,B,E,R,Q,E*x0,[0,Tsim]);






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Evaluting performance of optimal control with DAE-LTI
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------------------
% Step 1: Computing optimal J(V_opt,u_opt)=J(V,u) and V_opt, u_opt using 
%         sin(k *pi* upsilon) as basis
%--------------------------------------------------------------------------------------

GalerkinDim =dim;

[Pgalerkin,Kgalerkin,Agalerkin,Bgalerkin] = heatOptimalControlGalerkin(c,GalerkinDim,Nu);


real_cost=lambda^2*Pgalerkin(n,n); % J(V_{opt},u_{opt})

uxcoord = eye(GalerkinDim);

uxcoord = lambda*uxcoord(:,n); %% Coordinates in sin(k*pi*upsilon) of V_{opt}(0)

%Computing the coordinates $V_{opt}$ in sin(k*pi*upsilon) and u_{opt}
% V_{opt}(t)=\sum_{k=1}^{GalerkinDim} xstatesS3(t,k) sin(k*pi*upsilon)
[time3,xstatesS3] = ode45(@(t,x) (Agalerkin-Bgalerkin*Kgalerkin)*x,time,uxcoord);

uopt = -xstatesS3*Kgalerkin';

save('CoordinatesV_{opt}Insin(kxpi)','xstatesS3');
save('u_opt', 'uopt'); 


%----------------------------------------------------------------------------------------
% Step 2:     Comparing V_*=P_N^{+=a_* with V_opt  : compute 
%             || V_*(t) - V_opt(t)||^2_H by computing the square of the norm of
%             the difference between the coordinates of V_*(t) and V_opt(t) in
%             the  orthogonal basis sin(k*pi*upsilon
%---------------------------------------------------------------------------------------

% Computing the basis transformation matrix
ProjectionMatrix = zeros(dim,GalerkinDim);

for k=1:GalerkinDim
    for i=1:dim
            ProjectionMatrix(i,k)=inner_product(i, @(x) sin(k*pi*x),dim);
    end
end    

%Coordinates of V_*=P_N^{+}a_* in sine basis
DAEoptStatesinSin = xstates(:,1:dim)*ProjectionMatrix;
error3 = DAEoptStatesinSin - xstatesS3;
norm3 = diag(error3*error3');
  

figure
nsize=floor(size(time,1)/4);
plot(time(1:nsize), norm3(1:nsize), '--r');
%set(gca,'XTick',0:0.2:1);
set(gca,'FontSize', 16);
legend('||V_{*}-V_{opt}||^2_H');
hold on;


%----------------------------------------------------------------------------------------
% Step 3: Computing K_1
%-----------------------------------------------------------------------------------------
Kcl= K(:,1:dim)*scaling_Matrix;                    % Constructing K_1 (matrix repr. of the operator \mathcal{K}_f)


GalerkinDim = dim;

Kclsim = zeros(Nu,GalerkinDim);

% dot (Ex)=Ax+B*K*Ex=(A+B*K*E)x
% dot M_Na = -Lambda_N A_N a + Lambda_N e^m+B*Kf*M_N*a
% u = Kf *E*x = Kf*[I_N;0]'*Lambda_N * hatM*inv(hatM)*P_N*V = Kcl*P_N V 
% a' - coordinates w.r.t. sin(k*x*pi) => V=\sum_{k=1}^{dim} a_k'*sin(k*x*pi)
% P_N V =\sum_{k=1}^{N} a_k' P_N(sin(k*x*pi)

for k=1:GalerkinDim
    for i=1:dim
        Kclsim(:,k)=Kclsim(:,k)+Kcl(:,i)*inner_product(i, @(x) sin(k*pi*x),dim);
    end     
end


%----------------------------------------------------------------------------------------------------------%
%  Step 4:                                           Computing V_sim,u_sim
%-----------------------------------------------------------------------------------------------------------


Asim = (Agalerkin+Bgalerkin*Kclsim);   % A_e+B_eK_1





%Computingf V_sim(t)(upsilon)=\sum_{k=1}^{dimGalerkin}
%xstatesS1_k(t)sin(k*pi*upsilon), u_sim=uvalues1, 
% cost1(t)=J_t(V_sim,u_sim)=int_0^t norm(xstatesS1(tau))^2+norm(uvalues1(tau))^2ds

[time1, xstates1] = ode45(@(t,x) [Asim*x(1:GalerkinDim); x(1:GalerkinDim)'* (eye(GalerkinDim)+Kclsim'*Kclsim)*x(1:GalerkinDim)],time,[uxcoord;0]);

xstatesS1 = xstates1(:,1:GalerkinDim);
uvalues1 = xstatesS1*Kclsim';
cost1 = xstates1(:,end);


%-----------------------------------------------------------------------------------------
% Step 5:   Compare V_opt and V_sim
%-----------------------------------------------------------------------------------------

error1 = xstatesS1-xstatesS3; % Error between exact optimal trajectory and the trajectory induced by optimal feedback of DAE-LTI
norm1 = diag(error1*error1');

figure
plot(time, norm1, '-r');
set(gca,'FontSize', 16);
legend('||V_{sim}-V_{opt}||^2_H');
hold on;


%-------------------------------------------------------------------------------------------
% Step 6:  Compute optimal a_g,u_g and J_g(a_g,u_g) for Galerkin ODE
%--------------------------------------------------------------------------------------
% Optimal control for ODE-LTI obtained via Galerkin projection method
Aode = M\S;
Bode = M\B;
[KODE,PODE,e] = lqr(Aode, Bode,Mnoscaling,R,zeros(dim,Nu)); % Computing the optimal controller

cost_ODE=a0'*PODE*a0;  %Optimal cost J_g(q_g,u_g)

%compute a_g
[time21,xstates21] = ode45(@(t,x) (Aode-Bode*KODE)*x, time, a0);





%----------------------------------------------------------------------------------------
% Step 6: Compare V_{g}=P_N^{+} a_g and V_{opt}: compute 
%             || V_{g}(t) - V_opt(t)||^2_H by computing the square of the norm of
%             the difference between the coordinates of V_*(t) and V_opt(t) in
%             the  orthogonal basis sin(k*pi*upsilon
%-----------------------------------------------------------------------------------------



%optimal solution ODE in Legendre converted  into sine base
ODEoptStatesinSin  = xstates21*ProjectionMatrix;        

error4 = ODEoptStatesinSin - xstatesS3;
norm4 = diag(error4*error4');

figure;
plot(time, norm4, '--b', time, norm3, '-r');
legend('||V_{g}-V_{opt}||^2_H','||V_{*}-V_{opt}||^2_H');
set(gca,'FontSize', 16);
hold on;

%-------------------------------------------------------------------------------------------
% Step 7:  Compute K_{1,g} for Galerkin ODE
%--------------------------------------------------------------------------------------
Kclsim2 = zeros(Nu,GalerkinDim);
%Kclsim2=-Kgalerkin;

for k=1:GalerkinDim
        Kclsim2(:,k)=-KODE*projection(dim,@(x) sin(k*pi*x));
end    

Asim2 = (Agalerkin+Bgalerkin*Kclsim2);


%-------------------------------------------------------------------------------------------
% Step 8:  Compute coordinates of V_{sim,g}
%--------------------------------------------------------------------------------------

[time2, xstates2] = ode45(@(t,x) [Asim2*x(1:GalerkinDim);(x(1:GalerkinDim))'*(eye(GalerkinDim)+Kclsim2'*Kclsim2)*x(1:GalerkinDim)],time,[uxcoord;0]);

xstatesS2 = xstates2(:,1:GalerkinDim);
uvalues2 = xstatesS2*Kclsim2';
cost_sim_ODE = xstates2(:,end);


% Error between exact optimal trajectory and optimal trajectory induced by optimal feedback wit ODE-LTI
error2 = xstatesS2-xstatesS3; 
norm2 = diag(error2*error2');

figure;
plot(time, norm2, '--b', time, norm1, '-r');
set(gca,'FontSize', 16);
legend('||V_{sim,g}-V_{opt}||^2','||V_{sim}-V_{opt}||^2_H');
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display costs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('dim=%d, optimal_cost_DAE=%f, real_optimal_cost=%f, J(V_{sim},u_{sim}) = %f, J(V_g,u_g) =%f, J(V_{sim,g},u_{sim,g} =%f\n', dim,cost_DAE, real_cost,cost1(end),cost_ODE, cost_sim_ODE(end));



end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     Auxliary functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Computes the linear system linsys and the corresponding map LMAP associated with 
%the descriptor system d/dt (Ex)=Ax+Bu

function [linsys,LMAP]=DAE2Lin(E,A,B)
  [U,Sigma,V]=svd(E);

  c=size(A,1);
  n = size(A,2);
  m = size(B,2);

  invSigma = Sigma;
  r = rank(E);
%   r= 20;
  for i=1:r
	invSigma(i,i)=1/sqrt(Sigma(i,i));
  end

  invSigma1 = invSigma(1:r,1:r);
  invSigma2 = invSigma(1:r,1:r);
  if (n > r)
     invSigma1=blkdiag(invSigma1,eye(n-r,n-r));
  end   
  
  if (c > r)
     invSigma2=blkdiag(invSigma2,eye(c-r,c-r));
  end
  
%   size(invSigma2);
%   size(U);
  
  S=invSigma2*U';
  T=V*invSigma1;
  
  S*E*T;
  

  barA = S*A*T; 
  barB = S*B;
  hatA = barA(1:r,1:r);
  G    = [barA(1:r,r+1:n),barB(1:r,:)];
  C    = barA(r+1:c,1:r);
  D    = [barA(r+1:c,r+1:n),barB(r+1:c,:)];

  if (c > r) 
        [V,F] = vstar(hatA,G,C,D);

        V=ima(V);

  
        L=ints(invt(G,V),ker(D));
        
%         
%         disp('Rank L')
%         rank(L)
        
        Bl=[]; Dl=[];
        if (rank(L) ~= 0)
              Bl=V'*G*L;
              Dl = blkdiag(T,eye(m))*[zeros(size(V,1),size(L,2));L];
        end      
           
        if (rank(Dl) ~= size(Dl,2))
            disp('Dl should be full rank'); 
        end    
        
         Al= V'*(hatA+G*F)*V;
         Cl= blkdiag(T,eye(m))*[V;F*V];

        linsys=ss(Al,Bl,Cl,Dl);

        
  
  else
      k = size(G,2);
      linsys = ss (hatA,G,blkdiag(T,eye(m))*[eye(c);zeros(k,c)],[zeros(r,k);eye(k,k)]);
      
  end
  
  
  C=linsys.C;
        
  LMAP=pinv(E*C(1:n,:));
  
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Computes the stabilzable subspace V_stab of the system system_original and 
%returns the restriction of the original system to the subspace V.
%I.e. if the original system is (A,B,C,D) and V is the orthogonal basis of
%of V_stab, then it returns [V, (V'*A*V, V'*B, C*V,D)].

function [V,stab_sys] = Lin2StabLin(sys_original)

  A = sys_original.A;
  B = sys_original.B;
  C = sys_original.C;
  D = sys_original.D;

  [Xs,Xu,X0] = subsplit(A);

  Ctr  = mininv(A,B);

  V    = sums(Xs,Ctr);

  stab_sys = ss(V'*A*V,V'*B, C*V,D);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Returns the solution S of the algebraic Riccati equation, the state
%feedback K, the stabilizable associated system sys_stab and the
%corresponding map SLMAP for the infinite horizon optimal control problem
%lim x(t1)E'Q0Ex(t1)+int_0^{t_1} x'Qx+u'Ru dt. 

function [K,S,sys_stab,SLMAP] = DAEOptContrInf(E,A,B,Q,R)


[ss2lin, LMAP] =  DAE2Lin(E,A,B);

[V,sys_stab] = Lin2StabLin(ss2lin);

SLMAP = V'*LMAP;

k=size(sys_stab.B,2);
p = size(sys_stab.C,1);
n = size(A,2);
%[K,S,e] = lqry(sys_stab,blkdiag(Q,R),zeros(k,k),zeros(p,k));

Dn=sys_stab.d;
Qn = (sys_stab.c')*blkdiag(Q,R)*(sys_stab.c);
Sn = (sys_stab.c')*blkdiag(Q,R)*sys_stab.d;
Rn = Dn'*blkdiag(Q,R)*Dn;
En = eye(size(sys_stab.a,1));
[S,e,K]=care(sys_stab.a, sys_stab.b, Qn,Rn,Sn,En);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Based on the control law K, the associated stabilizable linear system ss2lin, and 
%the coresponding map LMAP, returned by
%DAEOPContrInf(A,E,B,Q,R), SimulateClLooopDAEInfOpt simulates the closed
%loop system on the interval [0,t1] starting from the initial state x0. It
%retunrs in time the time instances for which the states and outputs of the DAE are compute, in 
%xstates the corresponding state trajectory of the
%DAE, in uvalues the corresponding control input trajectories, and in cost
%the corresponding cost for the time instances given by time.
function [time,xstates,uvalues,cost,xstatesS,uvalues1,cost1] = SimulateClLoopDAEInfOpt(K,ss2lin,LMAP, A,B,E,R,Q,x0,t1)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

hatn = size(ss2lin.A,1);
n = size(A,2);
m = size(B,2);
nstate = size(E,1);

[time,y] = ode45(@SimulateClosedLoopSystem,t1,[LMAP*x0;0]);
 

 vinputs  = [];
 vstates  = [];
 xstates  = [];
 uvalues  = [];
 finalcost = [];
 
 for ts=1:size(time)
     vst     = y(ts,1:hatn)';
     vinp    = -K*vst;
     vinputs = [vinputs;vinp];
     vstates = [vstates; vst];
     outp =  ss2lin.C*vst+ss2lin.D*vinp;
     xstates  =  [xstates;outp(1:n)'];
     uvalues  = [uvalues;outp(n+1:n+m)'];
 end
 
 cost  = y(:,hatn+1);
 
 function dy=SimulateClosedLoopSystem(t,y)

          z=y(1:hatn);
          cost = y(hatn+1);
          
          u=-K*z;
          
          dz = ss2lin.A*z+ss2lin.B*u;
          
          output= ss2lin.C*z+ss2lin.D*u;
          
          %dcost = output'*blkdiag(Q,R)*output;
          
          dcost = output(1:n)'*Q*output(1:n)+output(n+1:end)'*R*output(n+1:end);
          
          dy=[dz;dcost];
          
 end   


end
function a = projection(dim, f)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% This function computes a vector a of projection coefficients associated to the given
% function f in the basis spanned by {P_{k+1} - P_{k-1}}_{k=1}^dim
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %


a = zeros(dim,1); 
for i = 1:dim
    a(i,1) = inner_product(i,f,dim); 
end;
M = mass_matrix_corrected_no_conditioning(dim); 
a = M\a; 

end
function M = mass_matrix_corrected(dim)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% This function determines the abscisas (x) and weights (w)  for the        %
% Gauss-Legendre quadrature, of order n>1, on the interval [-1, +1].        %
%   Unlike many publicly available functions, 'GaussLegendre_2' is valid    %
%   for n>=46. This is due to the fact that 'GaussLegendre_2' does not      %
%   rely on the build-in Matlab routine 'roots' to determine the roots of   %
%   the Legendre polynomial, but finds the roots by looking for the         %
%   eigenvalues of an alternative version of the companion matrix of the    %
%   n'th degree Legendre polynomial. The companion matrix is constructed    %
%   as a symmetrical matrix, guaranteeing that all the eigenvalues          %
%   (roots) will be real. On the contrary, the 'roots' function uses a      %
%   general form for the companion matrix, which becomes unstable at        %
%   higher orders n, leading to complex roots.                              %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %

M = zeros(dim); 
for k = 1:dim
    for n = 1:dim
        if(n == k+2)
            %M(k,n) = -2/(2*i+3);
              M(k,n) = -(2*k+1)/(2*k+3);
            %M(n,k) = -2/(2*n+3);
        end;
        if( n == k-2)
           M(k,n) = -(2*k+1)/(2*k-1);
           
        end;
        if (k == n) 
            %M(i,j) = 4*(2*i+1)/((2*i-1)*(2*i+3));
            M(k,n) = 2*(2*k+1)^2/((2*k-1)*(2*k+3));
        end;
    end;
end;
end

function A = stiffness_matrix_corrected(dim, c)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% This function determines the stiffness matrix for the Galerkin
% model: A_N = \{ <A \varphi_n,\varphi_k> \} 
%    = \{ < A P_n+1, P_k+1> - <A P_n+1, P_k-1> - <A P_n-1, P_k+1> +<A P_n-1, P_k-1> \}
% where < A P_n, P_k> = 0 if n+2 > k or n+2 \le k and k+n is odd. 
%Otherwise  < A P_n,P_k> = k(k+1) - n(n+1).  
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
d = 2*[1:dim]'+1;
d2 = d.*d/2; 
A = -2*c*diag(d2); 

end

function M = mass_matrix_corrected_no_conditioning(dim)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% This function determines the abscisas (x) and weights (w)  for the        %
% Gauss-Legendre quadrature, of order n>1, on the interval [-1, +1].        %
%   Unlike many publicly available functions, 'GaussLegendre_2' is valid    %
%   for n>=46. This is due to the fact that 'GaussLegendre_2' does not      %
%   rely on the build-in Matlab routine 'roots' to determine the roots of   %
%   the Legendre polynomial, but finds the roots by looking for the         %
%   eigenvalues of an alternative version of the companion matrix of the    %
%   n'th degree Legendre polynomial. The companion matrix is constructed    %
%   as a symmetrical matrix, guaranteeing that all the eigenvalues          %
%   (roots) will be real. On the contrary, the 'roots' function uses a      %
%   general form for the companion matrix, which becomes unstable at        %
%   higher orders n, leading to complex roots.                              %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %

M = zeros(dim); 
for k = 1:dim
    for n = 1:dim
        if(n == k+2)
            %M(k,n) = -2/(2*i+3);
              M(k,n) = -2/(2*k+3);
            %M(n,k) = -2/(2*n+3);
        end;
        if( n == k-2)
           M(k,n) = -2/(2*k-1);
           
        end;
        if (k == n) 
            %M(i,j) = 4*(2*i+1)/((2*i-1)*(2*i+3));
            M(k,n) = 4*(2*k+1)/((2*k-1)*(2*k+3));
        end;
    end;
end;
end
function ip = inner_product(n,f,dim)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% This function computes discrete inner product of the basis
% function \varphi_n = P_n+1 - P_n-1, P_n is Legendre polynomial of
% order n, and function f, that is
% \int_{-1}^1 \varphi_n(x) f(x) dx = \sum_{j=1}^{dim+2} P_{n+1}(x_j) f(x_j) w_j -
% \sum_{j=1}^{dim+2} P_{n-1}(x_j) f(x_j) w_j where {x_j} are roots of
% P_{dim+2} on (-1,1) , {w_j} are corresponding weights as in Hesthaven Spectral methods
% for time dependent problems, p.87, f. (5.25)  
% f is assumed to be an analytic function 
% dim = dimension of the reduced space
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %

% compute \sum_{j=1}^{n+2} P_{n+1}(x_j) f(x_j) w_j
[x,w] = GaussLegendre(dim+2);
Pn = legendre(n+1,x,'sch');
int1 = sum(Pn(1,:)'.*w.*f(x)); 
%compute \sum_{j=1}^n P_{n-1}(v_j) f(v_j) u_j 
Pn = legendre(n-1,x,'sch');
int2 = sum(Pn(1,:)'.*w.*f(x)); 

ip = int1 - int2; 
end

function [x, w] = GaussLegendre(n)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% This function determines the abscisas (x) and weights (w)  for the        %
% Gauss-Legendre quadrature, of order n>1, on the interval [-1, +1].        %
%   Unlike many publicly available functions, 'GaussLegendre_2' is valid    %
%   for n>=46. This is due to the fact that 'GaussLegendre_2' does not      %
%   rely on the build-in Matlab routine 'roots' to determine the roots of   %
%   the Legendre polynomial, but finds the roots by looking for the         %
%   eigenvalues of an alternative version of the companion matrix of the    %
%   n'th degree Legendre polynomial. The companion matrix is constructed    %
%   as a symmetrical matrix, guaranteeing that all the eigenvalues          %
%   (roots) will be real. On the contrary, the 'roots' function uses a      %
%   general form for the companion matrix, which becomes unstable at        %
%   higher orders n, leading to complex roots.                              %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %


% © Geert Van Damme
% geert@vandamme-iliano.be
% February 21, 2010    



% Building the companion matrix CM
    % CM is such that det(xI-CM)=P_n(x), with P_n the Legendre polynomial
    % under consideration. Moreover, CM will be constructed in such a way
    % that it is symmetrical.
i   = 1:n-1;
a   = i./sqrt(4*i.^2-1);
CM  = diag(a,1) + diag(a,-1);

% Determining the abscissas (x) and weights (w)
    % - since det(xI-CM)=P_n(x), the abscissas are the roots of the
    %   characteristic polynomial, i.d. the eigenvalues of CM;
    % - the weights can be derived from the corresponding eigenvectors.
[V L]   = eig(CM);
[x ind] = sort(diag(L));
V       = V(:,ind)';
w       = 2 * V(:,1).^2;
end
 function [P,K,A,B] = heatOptimalControlGalerkin(c,N,Nu)

%A=c^2 d/dx^2, A(sin(npix)=-c^2n^2pi^2 sin(npix)
A=-blkdiag(pi^2*c^2*(diag(1:N).^2));

B=[eye(Nu);zeros(N-Nu,Nu)];

Q=eye(N);
R=eye(Nu);
Nn=zeros(N,Nu);

[K,P,e]=lqr(A,B,Q,R,Nn);

 end

function matrix2latex(filename, matrix, varargin)

% function: matrix2latex(...)
% Author:   M. Koehler
% Contact:  koehler@in.tum.de
% Version:  1.1
% Date:     May 09, 2004

% This software is published under the GNU GPL, by the free software
% foundation. For further reading see: http://www.gnu.org/licenses/licenses.html#GPL

% Usage:
% matrix2late(matrix, filename, varargs)
% where
%   - matrix is a 2 dimensional numerical or cell array
%   - filename is a valid filename, in which the resulting latex code will
%   be stored
%   - varargs is one ore more of the following (denominator, value) combinations
%      + 'rowLabels', array -> Can be used to label the rows of the
%      resulting latex table
%      + 'columnLabels', array -> Can be used to label the columns of the
%      resulting latex table
%      + 'alignment', 'value' -> Can be used to specify the alginment of
%      the table within the latex document. Valid arguments are: 'l', 'c',
%      and 'r' for left, center, and right, respectively
%      + 'format', 'value' -> Can be used to format the input data. 'value'
%      has to be a valid format string, similar to the ones used in
%      fprintf('format', value);
%      + 'size', 'value' -> One of latex' recognized font-sizes, e.g. tiny,
%      HUGE, Large, large, LARGE, etc.
%
% Example input:
%   matrix = [1.5 1.764; 3.523 0.2];
%   rowLabels = {'row 1', 'row 2'};
%   columnLabels = {'col 1', 'col 2'};
%   matrix2latex(matrix, 'out.tex', 'rowLabels', rowLabels, 'columnLabels', columnLabels, 'alignment', 'c', 'format', '%-6.2f', 'size', 'tiny');
%
% The resulting latex file can be included into any latex document by:
% /input{out.tex}
%
% Enjoy life!!!

    rowLabels = [];
    colLabels = [];
    alignment = 'l';
    format = [];
    textsize = [];
    if (rem(nargin,2) == 1 || nargin < 2)
        error('matrix2latex: ', 'Incorrect number of arguments to %s.', mfilename);
    end

    okargs = {'rowlabels','columnlabels', 'alignment', 'format', 'size'};
    for j=1:2:(nargin-2)
        pname = varargin{j};
        pval = varargin{j+1};
        k = strmatch(lower(pname), okargs);
        if isempty(k)
            error('matrix2latex: ', 'Unknown parameter name: %s.', pname);
        elseif length(k)>1
            error('matrix2latex: ', 'Ambiguous parameter name: %s.', pname);
        else
            switch(k)
                case 1  % rowlabels
                    rowLabels = pval;
                    if isnumeric(rowLabels)
                        rowLabels = cellstr(num2str(rowLabels(:)));
                    end
                case 2  % column labels
                    colLabels = pval;
                    if isnumeric(colLabels)
                        colLabels = cellstr(num2str(colLabels(:)));
                    end
                case 3  % alignment
                    alignment = lower(pval);
                    if alignment == 'right'
                        alignment = 'r';
                    end
                    if alignment == 'left'
                        alignment = 'l';
                    end
                    if alignment == 'center'
                        alignment = 'c';
                    end
                    if alignment ~= 'l' && alignment ~= 'c' && alignment ~= 'r'
                        alignment = 'l';
                        warning('matrix2latex: ', 'Unkown alignment. (Set it to \''left\''.)');
                    end
                case 4  % format
                    format = lower(pval);
                case 5  % format
                    textsize = pval;
            end
        end
    end

    fid = fopen(filename, 'w');
    
    width = size(matrix, 2);
    height = size(matrix, 1);

    if isnumeric(matrix)
        matrix = num2cell(matrix);
        for h=1:height
            for w=1:width
                if(~isempty(format))
                    matrix{h, w} = num2str(matrix{h, w}, format);
                else
                    matrix{h, w} = num2str(matrix{h, w});
                end
            end
        end
    end
    
    if(~isempty(textsize))
        fprintf(fid, '\\begin{%s}', textsize);
    end

    fprintf(fid, '\\begin{tabular}{|');

    if(~isempty(rowLabels))
        fprintf(fid, 'l|');
    end
    for i=1:width
        fprintf(fid, '%c|', alignment);
    end
    fprintf(fid, '}\r\n');
    
    fprintf(fid, '\\hline\r\n');
    
    if(~isempty(colLabels))
        if(~isempty(rowLabels))
            fprintf(fid, '&');
        end
        for w=1:width-1
            fprintf(fid, '\\textbf{%s}&', colLabels{w});
        end
        fprintf(fid, '\\textbf{%s}\\\\\\hline\r\n', colLabels{width});
    end
    
    for h=1:height
        if(~isempty(rowLabels))
            fprintf(fid, '\\textbf{%s}&', rowLabels{h});
        end
        for w=1:width-1
            fprintf(fid, '%s&', matrix{h, w});
        end
        fprintf(fid, '%s\\\\\\hline\r\n', matrix{h, width});
    end

    fprintf(fid, '\\end{tabular}\r\n');
    
    if(~isempty(textsize))
        fprintf(fid, '\\end{%s}', textsize);
    end

    fclose(fid);

end
